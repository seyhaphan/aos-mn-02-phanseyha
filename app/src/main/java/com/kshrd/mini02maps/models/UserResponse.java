package com.kshrd.mini02maps.models;

public class UserResponse {
    private String email,username;

    public UserResponse() {
    }

    public UserResponse(String email, String username) {
        this.email = email;
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
