package com.kshrd.mini02maps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kshrd.mini02maps.ui.singin.SignInFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser != null){
            Intent intent = new Intent(MainActivity.this,HomeDrawerActivity.class);
            intent.putExtra("USERID",firebaseUser.getUid());
            startActivity(intent);
            finish();

        }else{
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container,new SignInFragment())
                    .commit();
        }
    }
}