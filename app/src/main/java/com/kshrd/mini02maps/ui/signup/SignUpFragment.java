package com.kshrd.mini02maps.ui.signup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.kshrd.mini02maps.HomeDrawerActivity;
import com.kshrd.mini02maps.R;
import com.kshrd.mini02maps.models.UserRequest;
import com.kshrd.mini02maps.ui.singin.SignInFragment;

public class SignUpFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = SignUpFragment.class.getSimpleName();
    private FirebaseAuth firebaseAuth;
    private ProgressBar progressBar;
    private TextInputEditText edEmail,edPassword,edUsername;
    private Button btnSingUp;
    private TextView tvSingIn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_signup,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.progress_circular);
        edEmail = view.findViewById(R.id.edEmail);
        edPassword = view.findViewById(R.id.edPasswd);
        edUsername = view.findViewById(R.id.edUsername);
        tvSingIn = view.findViewById(R.id.tvSingIn);
        btnSingUp = view.findViewById(R.id.btnSingUp);

        tvSingIn.setOnClickListener(this);
        btnSingUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSingUp:

                String email = edEmail.getText().toString();
                String pass = edPassword.getText().toString();
                String username = edUsername.getText().toString();

                setLoading(true);

                createUser(new UserRequest(email,pass,username));

                break;

            case R.id.tvSingIn:
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container,new SignInFragment())
                        .commit();
                break;
        }
    }

    private void setLoading(boolean bool){
        if(bool == true)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void createUser(UserRequest userRequest){

        firebaseAuth.createUserWithEmailAndPassword(userRequest.getEmail(), userRequest.getPassword())
                .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseDatabase.getInstance().getReference("users")
                                    .child(firebaseAuth.getInstance().getUid())
                                    .setValue(userRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(getContext(),"Register successfully",Toast.LENGTH_SHORT).show();

                                        Intent intent = new Intent(getActivity(), HomeDrawerActivity.class);
                                        intent.putExtra("USERID",firebaseAuth.getCurrentUser().getUid());
                                        startActivity(intent);
                                        getActivity().finish();
                                    }else{
                                        Toast.makeText(getContext(),"Register failed",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                        } else {
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        setLoading(false);
                    }
                });
    }


}
