package com.kshrd.mini02maps.ui.singin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.kshrd.mini02maps.HomeDrawerActivity;
import com.kshrd.mini02maps.R;
import com.kshrd.mini02maps.ui.signup.SignUpFragment;

public class SignInFragment extends Fragment implements View.OnClickListener{
    private static final String TAG = SignInFragment.class.getSimpleName();
    private FirebaseAuth firebaseAuth;
    private ProgressBar progressBar;
    private Button btnSignIn;
    private TextView tvSignUp;
    private TextInputEditText edEmail,edPassword;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signin,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.progress_circular);
        btnSignIn = view.findViewById(R.id.btnSingIn);
        tvSignUp = view.findViewById(R.id.tvSingUp);
        edEmail = view.findViewById(R.id.edEmail);
        edPassword = view.findViewById(R.id.edPasswd);

        btnSignIn.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSingIn:
                String email = edEmail.getText().toString();
                String password = edPassword.getText().toString();
                setLoading(true);
                signInRequest(email, password);
                break;

            case R.id.tvSingUp:
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new SignUpFragment())
                        .commit();
                break;
        }
    }

    private void setLoading(boolean bool){
        if(bool == true)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void signInRequest(String email, String password) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Intent intent = new Intent(getContext(), HomeDrawerActivity.class);
                            intent.putExtra("USERID",firebaseAuth.getCurrentUser().getUid());
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        setLoading(false);
                    }
                });
    }


}
